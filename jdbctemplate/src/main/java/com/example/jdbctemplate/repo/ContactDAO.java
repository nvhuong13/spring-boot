package com.example.jdbctemplate.repo;

import com.example.jdbctemplate.model.Contact;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ContactDAO {

    public void saveOrUpdate(Contact contact);

    public void delete(int contactId);

    public Contact get(int contactId);

    public List<Contact> list();
}