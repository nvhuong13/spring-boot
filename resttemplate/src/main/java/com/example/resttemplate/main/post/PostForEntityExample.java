package com.example.resttemplate.main.post;

import com.example.resttemplate.model.Employee;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class PostForEntityExample {

    static final String URL_CREATE_EMPLOYEE = "http://localhost:2000/employee";

    public static void main(String[] args) {

        Employee newEmployee = new Employee("E11", "Tom", "Cleck");

        RestTemplate restTemplate = new RestTemplate();

        // Dữ liệu đính kèm theo yêu cầu.
        HttpEntity<Employee> requestBody = new HttpEntity<>(newEmployee);

        // Gửi yêu cầu với phương thức POST.
        ResponseEntity<Employee> result
                = restTemplate.postForEntity(URL_CREATE_EMPLOYEE, requestBody, Employee.class);

        System.out.println("Status code:" + result.getStatusCode());

        // Code = 200.
        if (result.getStatusCode() == HttpStatus.OK) {
            Employee e = result.getBody();
            System.out.println("(Client Side) Employee Created: "+ e.getEmpNo());
        }

        //

        // Gửi yêu cầu với phương thức POST.
        ResponseEntity<Object> result2
                = restTemplate.postForEntity(URL_CREATE_EMPLOYEE, requestBody, Object.class);

        System.out.println("Status code:" + result.getStatusCode());

        // Code = 200.
        if (result2.getStatusCode() == HttpStatus.OK) {
            System.out.println("(Client Side) Employee Created: "+ result2.getBody());
        }
    }
}
