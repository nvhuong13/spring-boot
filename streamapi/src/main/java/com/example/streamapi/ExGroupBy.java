package com.example.streamapi;

import com.example.streamapi.model.Person;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ExGroupBy {
    public static void main(String[] args) {
        ex1();
    }

    public static void ex1() {
        List<Person> list = Arrays.asList(
                new Person("name1", 12)
                ,new Person("name2", 42)
                ,new Person("name1", 15)
                ,new Person("name3", 31));

        Map<String, List<Person>> result = list.stream().collect(Collectors.groupingBy(Person::getName));
    }
}
