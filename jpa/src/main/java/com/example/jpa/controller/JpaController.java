package com.example.jpa.controller;

import com.example.jpa.entity.ContactDetailEntity;
import com.example.jpa.entity.ContactEntity;
import com.example.jpa.model.IContact;
import com.example.jpa.repo.JpaTestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@RestController
public class JpaController {

    @Autowired
    JpaTestRepository jpaTestRepository;

    @GetMapping("testJpa1")
    public Object test1() {
        List<ContactEntity> lst = jpaTestRepository.findAllByName("name contact");
        return lst;
    }


    @GetMapping("testJpa2")
    public Object test2() {
//        List<ContactEntity> lst = jpaTestRepository.findAllByNameLike("%name contact%");
//        List<ContactEntity> lst = jpaTestRepository.findAllByNameLikeQuery1("%name contact%");
//        List<IContact> lst = jpaTestRepository.findAllByNameLikeQuery2("%name contact%");

        List<Object[]> lst = jpaTestRepository.findAllByNameLikeQuery3("%name contact%");
        List<ContactEntity> lstResult = new ArrayList<>();
        for (int i = 0; i < lst.size(); i++) {
            ContactEntity contactEntity = new ContactEntity();
            contactEntity.setContactId(((BigInteger) lst.get(i)[0]).longValue());
            contactEntity.setName((String) lst.get(i)[1]);
            contactEntity.setEmail((String) lst.get(i)[2]);
            contactEntity.setAddress((String) lst.get(i)[3]);
            contactEntity.setTelephone((String) lst.get(i)[4]);
            lstResult.add(contactEntity);

        }
        
        List<ContactDetailEntity> lstDetail = jpaTestRepository.selectAllContactDetail("%name contact%");
        System.out.println(lstDetail);
        return lstResult;
    }
}
