package com.example.resttemplate.main.delete;


import com.example.resttemplate.model.Employee;
import org.springframework.web.client.RestTemplate;

public class DeleteExample2 {

    public static void main(String[] args) {

        RestTemplate restTemplate = new RestTemplate();

        // URL với biến URI (URI variable)
        String resourceUrl = "http://localhost:2000/employee/{empNo}";
        String resourceUrlGet = "http://localhost:2000/employee/E02";
        Object[] uriValues = new Object[] { "E02" };

        // Gửi yêu cầu với phương thức DELETE.
        restTemplate.delete(resourceUrl, uriValues);

        Employee e = restTemplate.getForObject(resourceUrlGet, Employee.class);

        if (e != null) {
            System.out.println("(Client side) Employee after delete: ");
            System.out.println("Employee: " + e.getEmpNo() + " - " + e.getEmpName());
        } else {
            System.out.println("Employee not found!");
        }
    }

}