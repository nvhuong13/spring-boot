package com.example.streamapi;

import com.example.streamapi.model.Person;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ExFilter {
    static List<Integer> numbers = Arrays.asList(7, 2, 5, 4, 2, 1);

    public static void main(String[] args) {
        withoutStream();
        withStream();
        filerStream();
        filterObject();
    }

    public static void withoutStream() {
        long count = 0;
        for (Integer number : numbers) {
            if (number % 2 == 0) {
                count++;
            }
        }
        System.out.printf("\nThere are %d elements that are even", count);
    }

    public static void withStream() {
        long count = numbers.stream().filter(num -> num % 2 == 0).count();
        System.out.printf("\nThere are %d elements that are even", count);
    }

    public static void filerStream() {
        System.out.println();
        List<Integer> numbersResult = numbers.stream().filter(num -> num % 2 == 0).collect(Collectors.toList());
        for (Integer i: numbersResult) {
            System.out.println(i);
        }
    }

    public static void filterObject() {
        List<Person> persons = Arrays.asList(
                new Person("name1", 12),
                new Person("name2", 20),
                new Person("name3", 50)
        );

        Person result1 = persons.stream()
                .filter(x -> "name2".equals(x.getName()))
                .findAny()
                .orElse(null);

        Person result2 = persons.stream()
                .filter(p -> {
                    if ("name2".equals(p.getName()) && 20 == p.getAge()) {
                        return true;
                    }
                    return false;
                }).findAny()
                .orElse(null);
    }
}
