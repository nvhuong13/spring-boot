package com.example.jpa.model;

public interface IContact {
    String getContactId();
    String getName();
    String getEmail();
    String getAddress();
    String getTelephone();
}
