package com.example.streamapi.model;

public class Programing {
    private String name;
    private int exp;

    public Programing(String name, int age) {
        super();
        this.name = name;
        this.exp = age;
    }

    public String getName() {
        return name;
    }

    public int getExp() {
        return exp;
    }
}
