package com.example.multidatabasejpa;

import com.example.multidatabasejpa.dao.product.ProductRepository;
import com.example.multidatabasejpa.dao.user.UserRepository;
import com.example.multidatabasejpa.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import java.util.List;

@RestController
public class TestController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductRepository productRepository;

    EntityManager entityManager;

    public TestController(@Qualifier("productEntityManager") EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @GetMapping("test1")
    public User test1() {
        User user1 = new User();
        user1.setName("John");
        user1.setEmail("john@test.com");
        user1.setAge(20);
        user1 = userRepository.save(user1);
        return user1;
    }


    @GetMapping("test2")
    public Object test2() {
        Query query = entityManager.createNativeQuery("select * from product");
        List<Object[]> results = query.getResultList();
        return results.get(0)[1];
    }
}
