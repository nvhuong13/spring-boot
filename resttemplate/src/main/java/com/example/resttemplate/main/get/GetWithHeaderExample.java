package com.example.resttemplate.main.get;

import java.util.Arrays;

import com.example.resttemplate.model.Employee;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class GetWithHeaderExample {

    static final String URL_EMPLOYEES = "http://localhost:2000/employees";

    public static void main(String[] args) {

        // HttpHeaders
        HttpHeaders headers = new HttpHeaders();

        headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
        // Yêu cầu trả về định dạng JSON
        headers.setContentType(MediaType.APPLICATION_JSON);

        // HttpEntity<String>: To get result as String.
        HttpEntity<String> entity = new HttpEntity<String>(headers);

        // RestTemplate
        RestTemplate restTemplate = new RestTemplate();

        System.out.println("===========object===========");
        // Gửi yêu cầu với phương thức GET, và các thông tin Headers.
        ResponseEntity<Object> response = restTemplate.exchange(URL_EMPLOYEES, //
                HttpMethod.GET, entity, Object.class);
        System.out.println(response.getBody().toString());

        System.out.println("===========string===========");
        // Gửi yêu cầu với phương thức GET, và các thông tin Headers.
        ResponseEntity<String> responseString = restTemplate.exchange(URL_EMPLOYEES, //
                HttpMethod.GET, entity, String.class);
        System.out.println(responseString.getBody());

        System.out.println("===========Employee[]===========");
        // Gửi yêu cầu với phương thức GET và Headers mặc định.
        Employee[] list = restTemplate.getForObject(URL_EMPLOYEES, Employee[].class);

        if (list != null) {
            for (Employee e : list) {
                System.out.println("Employee: " + e.getEmpNo() + " - " + e.getEmpName());
            }
        }
    }

}