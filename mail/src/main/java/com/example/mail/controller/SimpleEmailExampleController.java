package com.example.mail.controller;

import com.example.mail.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SimpleEmailExampleController {

    @Autowired
    public JavaMailSender emailSender;

    @Value("${mail.recipt}")
    String mailRecipt;

    @Autowired
    EmailService emailService;

    @ResponseBody
    @RequestMapping("/sendSimpleEmail")
    public String sendSimpleEmail() {
        try {
            // Create a Simple MailMessage.
            SimpleMailMessage message = new SimpleMailMessage();

            message.setTo(mailRecipt);
            message.setSubject("Test Simple Email");
            message.setText("Hello, Im 1041 testing Simple Email\n\n\n" +
                    "Hello, Im 1041 testing Simple Email\n\n" +
                    "\n");

            // Send Message!
            this.emailSender.send(message);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            return "Email Not Sent!";
        }


        return "Email Sent!";
    }

    @ResponseBody
    @RequestMapping("/sendSimpleEmail2")
    public String sendSimpleEmail2() {
        try {
            emailService.sendMail("huongnv.iist@gmail.com", "tifsdfsdftle", "cosdfdsfn\n\n\ntent");
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            return "Email Not Sent!";
        }

        return "Email Sent!";
    }

}