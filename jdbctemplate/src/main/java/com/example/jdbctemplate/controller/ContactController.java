package com.example.jdbctemplate.controller;

import com.example.jdbctemplate.model.Contact;
import com.example.jdbctemplate.repo.ContactDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ContactController {
    @Autowired
    private ContactDAO contactDAO;

    @GetMapping("save")
    public List<Contact> save() {
        contactDAO.saveOrUpdate(new Contact("Tumi Le", "huongnv@gmail.com", "abc","0123"));
        List<Contact> contacts = contactDAO.list();

        return contacts;
    }
}
