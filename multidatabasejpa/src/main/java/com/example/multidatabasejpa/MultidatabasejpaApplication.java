package com.example.multidatabasejpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultidatabasejpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MultidatabasejpaApplication.class, args);
	}

}
