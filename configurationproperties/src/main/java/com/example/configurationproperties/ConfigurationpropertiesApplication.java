package com.example.configurationproperties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties // enable config properties
public class ConfigurationpropertiesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfigurationpropertiesApplication.class, args);
	}

}
