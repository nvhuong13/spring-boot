package com.example.resttemplate.main.post;
import com.example.resttemplate.model.Employee;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
public class PostForObjectExample {

    static final String URL_CREATE_EMPLOYEE = "http://localhost:2000/employee";

    public static void main(String[] args) {

        String empNo = "E11";

        Employee newEmployee = new Employee(empNo, "Tom", "Cleck");

        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_XML_VALUE);
        headers.setContentType(MediaType.APPLICATION_XML);

        RestTemplate restTemplate = new RestTemplate();

        // Dữ liệu đính kèm theo yêu cầu.
        HttpEntity<Employee> requestBody = new HttpEntity<>(newEmployee, headers);

        // Gửi yêu cầu với phương thức POST.
        Employee e = restTemplate.postForObject(URL_CREATE_EMPLOYEE, requestBody, Employee.class);

        if (e != null && e.getEmpNo() != null) {
            System.out.println("Employee created: " + e.toString());
        } else {
            System.out.println("Something error!");
        }

        //
        Object eObj = restTemplate.postForObject(URL_CREATE_EMPLOYEE, requestBody, Object.class);

        if (eObj != null) {
            System.out.println("Employee created: " + eObj.toString());
        } else {
            System.out.println("Something error!");
        }

    }
}
