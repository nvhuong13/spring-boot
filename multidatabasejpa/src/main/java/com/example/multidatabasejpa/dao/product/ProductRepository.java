package com.example.multidatabasejpa.dao.product;

import com.example.multidatabasejpa.model.product.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Integer> {


}