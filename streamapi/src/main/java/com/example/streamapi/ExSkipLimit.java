package com.example.streamapi;

import java.util.Arrays;
import java.util.List;

public class ExSkipLimit {
    public static void main(String[] args) {
        ex1();

    }
    public static void ex1() {
        List<String> data = Arrays.asList("Java", "C#", "C++", "PHP", "Javascript");
        System.out.println("-----Skip 1: Limit 3");
        data.stream()
                .skip(1)
                .limit(3)
                .forEach(System.out::println); // C#C++PHP
    }
}
