package com.example.configurationproperties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Data
@Component // 1 spring bean
@PropertySource("classpath:huongnv.properties") // Mark get config from file huongnv.properties, if not found application.properties
@ConfigurationProperties(prefix = "url") // Just get config have prefix "url"
public class ConfigUrlProperties {
    private String google;
    private String youtube;
}