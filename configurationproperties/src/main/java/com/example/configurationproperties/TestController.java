package com.example.configurationproperties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class TestController {

    @Autowired
    ConfigUrlProperties configUrlProperties;

    @GetMapping("test")
    public List<String> test() {
        List<String> list = new ArrayList<>();
        list.add(configUrlProperties.getGoogle());
        list.add(configUrlProperties.getYoutube());
        return list;
    }
}
