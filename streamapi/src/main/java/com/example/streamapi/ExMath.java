package com.example.streamapi;

import com.example.streamapi.model.Programing;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class ExMath {
    public static void main(String[] args) {
        // ex1();
        ex2();
    }

    public static void ex1() {
        Integer []numbers = {1, 8, 3, 4, 5, 7, 9, 6};

        // Find max, min with Array ====================

        // Max = 9
        Integer maxNumber = Stream.of(numbers)
                .max(Comparator.comparing(Integer::valueOf))
                .get();
        System.out.println(maxNumber);

        // Min = 1
        Integer minNumber = Stream.of(numbers)
                .min(Comparator.comparing(Integer::valueOf))
                .get();
        // Find max, min with Collection ====================
        List<Integer> listOfIntegers = Arrays.asList(numbers);
        System.out.println(minNumber);

        // Max = 9
        Integer max = listOfIntegers.stream()
                .mapToInt(v -> v)
                .max()
                .getAsInt();
        System.out.println(max);
        // Min = 1
        Integer min = listOfIntegers.stream()
                .mapToInt(v -> v)
                .min()
                .getAsInt();
        System.out.println(min);
    }

    public static void ex2() {
        List<Programing> students = Arrays.asList( //
                new Programing("Java", 5), //
                new Programing("PHP", 2), //
                new Programing("C#", 1) //
        );

        // Max = 5
        Programing maxByExp = students.stream()
                .max(Comparator.comparing(Programing::getExp))
                .get();
        System.out.println(maxByExp.getExp());

        // Min = 1
        Programing minByExp = students.stream()
                .min(Comparator.comparing(Programing::getExp))
                .get();
        System.out.println(minByExp.getExp());
    }
}
