package com.example.streamapi;

import java.util.Arrays;
import java.util.List;

public class ExMatch {
    public static void main(String[] args) {
        ex1();
    }

    public static void ex1() {
        List<String> data = Arrays.asList("Java", "C#", "C++", "PHP", "Javascript");
        boolean result = data.stream().anyMatch((s) -> s.equalsIgnoreCase("Java")); // return true if it has any match condition
        System.out.println(result); // true

        // allMatch: return true if all data match condition
        // noneMatch: return true if don't have any match condition
    }
}
