package com.example.streamapi;

import com.example.streamapi.model.Person;

import java.util.Arrays;
import java.util.List;

public class ExSorted {
    public static void main(String[] args) {
        ex1();
        ex2();
    }

    public static void ex1() {
        List<String> data = Arrays.asList("Java", "C#", "C++", "PHP", "Javascript");

        // sorted according to natural order
        data.stream() //
                .sorted() //
                .forEach(System.out::println);
        System.out.println("-------------------------------------------");
        // sorted according to the provided Comparator
        data.stream() //
                .sorted((s1, s2) -> s2.length() - s1.length()) //
                .forEach(System.out::println);
    }

    public static void ex2() {
        List<Person> list = Arrays.asList(
                new Person("name1", 25),
                new Person("name2", 55),
                new Person("name3", 15));

        list.stream()
                .sorted((s1, s2) -> {
                    if (s2.getAge() - s1.getAge() > 0) {
                        return 0;
                    }
                    return -1;
                })
                .forEach(System.out::println);
    }
}
