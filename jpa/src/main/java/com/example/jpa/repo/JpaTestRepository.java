package com.example.jpa.repo;

import com.example.jpa.entity.ContactDetailEntity;
import com.example.jpa.entity.ContactEntity;
import com.example.jpa.model.IContact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JpaTestRepository extends JpaRepository<ContactEntity, Long> {

    List<ContactEntity> findAllByName(String name);

    List<ContactEntity> findAllByNameLike(String name);

    @Query("select c from ContactEntity c where c.name like :name ")
    List<ContactEntity> findAllByNameLikeQuery1(@Param("name") String name);

    @Query(value =
            " select contact_id as contactId, " +
            " name as name," +
            " email as email," +
            " address as address," +
            " telephone as telephone " +
            " from contact where name like :name ", nativeQuery = true)
    List<IContact> findAllByNameLikeQuery2(@Param("name") String name);

    @Query(value =
            " select contact_id, " +
                    " name," +
                    " email," +
                    " address," +
                    " telephone " +
                    " from contact where name like :name ", nativeQuery = true)
    List<Object[]> findAllByNameLikeQuery3(@Param("name") String name);


    @Query("select cd from ContactEntity c join ContactDetailEntity cd on c.contactId = cd.contactId where c.name like :name")
    List<ContactDetailEntity> selectAllContactDetail(@Param("name") String name);
}
