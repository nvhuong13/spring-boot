package com.example.streamapi;

import com.example.streamapi.model.Person;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ExMap {
    public static void main(String[] args) {
        // ex1();
        ex2();
    }

    public static void ex1() {
        List<String> data = Arrays.asList("Java", "C#", "C++", "PHP", "Javascript");

        data.stream()
                .map(String::toUpperCase) // convert each element to upper case
                .forEach(System.out::println);
    }

    public static void ex2() {
        List<Person> list = Arrays.asList(
                new Person("name1", 25),
                new Person("name2", 55));
//        Map<String, Person> map = list.stream()
//                .collect(Collectors.toMap(Person::getName, Function.identity()));
//        for (Map.Entry<String, Person> entry : map.entrySet()) {
//            System.out.println(entry.getKey() + "/" + entry.getValue());
//        }

        Map<String, Integer> map = list.stream()
                .collect(Collectors.toMap(x -> x.getName(), x -> x.getAge()));
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            System.out.println(entry.getKey() + "/" + entry.getValue());
        }
    }
}
