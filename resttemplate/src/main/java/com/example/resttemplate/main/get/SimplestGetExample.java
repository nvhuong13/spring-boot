package com.example.resttemplate.main.get;

import com.example.resttemplate.model.Employee;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class SimplestGetExample {

    static final String URL_EMPLOYEES = "http://localhost:2000/employees";

    static final String URL_EMPLOYEES_XML = "http://localhost:2000/employees.xml";
    static final String URL_EMPLOYEES_JSON = "http://localhost:2000/employees.json";

    public static void main(String[] args) {

        RestTemplate restTemplate = new RestTemplate();

        // Gửi yêu cầu với phương thức GET và Headers mặc định.
        String result = restTemplate.getForObject(URL_EMPLOYEES, String.class);
        System.out.println(result);

    }

}