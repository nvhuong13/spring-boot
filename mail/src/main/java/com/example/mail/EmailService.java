package com.example.mail;

import com.sun.org.slf4j.internal.Logger;
import com.sun.org.slf4j.internal.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Date;
import java.util.Properties;

@Component
public class EmailService {

    private final Logger log = LoggerFactory.getLogger(EmailService.class);

    @Value("${mail.smtp.host}")
    private String host;
    @Value("${mail.smtp.port}")
    private String port;
    @Value("${mail.smtp.username}")
    private String username;
    @Value("${mail.smtp.password}")
    private String password;

    public void sendMail(String to, String title, String body) {
        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.localhost", host);
        props.put("mail.smtp.socketFactory.port", port);
        props.put("mail.smtp.ssl.enable", true);
        props.put("mail.smtp.ssl.trust", "*");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", port);
//        props.put("mail.smtp.proxy.host", "192.168.5.254");
//        props.put("mail.smtp.proxy.port", "8080");
//        props.put("mail.smtp.proxy.user", "tempvnhn");
//        props.put("mail.smtp.proxy.password", "VNHNgL5P");
        Session session = getSession(props, username, password);

        final Message msg = new MimeMessage(session);

        try {
            msg.setFrom(new InternetAddress(username));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to, false));
            msg.setSubject(title);
            msg.setSentDate(new Date());
            Multipart multipart = new MimeMultipart();
            MimeBodyPart messagePart = new MimeBodyPart();
            messagePart.setContent(body, "text/plain; charset=UTF-8");
            multipart.addBodyPart(messagePart);
            msg.setContent(multipart);
            msg.setHeader("charset", "UTF-8");
            Transport.send(msg);
            log.debug("Sent message successfully....");
        } catch (MessagingException mex) {
            log.error("send failed, exception: {}", mex);
        }
    }

    private Session getSession(Properties prop, String userMail, String passMail) {
        Session session = Session.getDefaultInstance(prop,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(userMail, passMail);
                    }
                });
        return session;
    }

    public static void main(String[] args) {
        EmailService email2Service = new EmailService();
        email2Service.sendMail("huongnv.iist@gmail.com", "subject", "content");
    }
}